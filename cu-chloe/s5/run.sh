#!/bin/bash

. ./cmd.sh
. ./path.sh

feats_nj=5
test_nj=2
decode_nj=8
stage=0

CU_CHLOE=/home/leo/CU-CHLOE

if [ $stage -le 0 ]; then
	# remove previous data
	[ -d data/test ] && rm -rf data/test
	echo ============================================================================
	echo "                            Data Preparing                                "
	echo ============================================================================
	./cu-chloe_data_prep.sh $CU_CHLOE || exit 1
	echo "Data preparing succeed!"

	# make sorting
	utils/fix_data_dir.sh data/test || exit 1
	echo "Please modify the text in data/test, and then make 'stage=1', run this script again!"
	echo "Thank you!"
	exit 0
fi

if [ $stage -le 1 ]; then
	echo ============================================================================
	echo "                      MFCC feature Extraction                            "
	echo ============================================================================
	[ -d mfcc ] && rm -rf mfcc
	[ -d exp/make_mfcc ] && rm -rf exp/make_mfcc
	# make MFCC features
	mfccdir=mfcc

	steps/make_mfcc.sh --cmd "$train_cmd" --nj $feats_nj data/test exp/make_mfcc/test $mfccdir || exit 1
	steps/compute_cmvn_stats.sh data/test exp/make_mfcc/test $mfccdir || exit 1

	echo "MFCC Feature Extraction & CMVN done!"
	exit 0
fi

if [ $stage -le 2 ]; then
	echo ============================================================================
	echo "                 Combine MFCC features with Qdict                         "
	echo ============================================================================
	[ -d co-feats ] && rm -rf co-feats
	[ -d exp/mono_ali_test ] && rm -rf exp/mono_ali_test
	[ ! -d co-feats ] && mkdir co-feats
	# convert ark,scp:feats to txt:feats
	./self-code/convert_ark2txt.sh co-feats || exit 1
	echo "convert ark,scp:feats to txt:feats done!"

	# make canonical transcriptions by forced alignment
	steps/align_si.sh --boost-silence 1.25 --nj "$test_nj" --cmd "$train_cmd" \
	 data/test data/lang exp/mono exp/mono_ali_test || exit 1

	echo "Forced alignment done!"


	# make label
	./self-code/make_label.sh exp/mono_ali_test co-feats "$test_nj" || exit 1
	# make binary senquence
	python self-code/create_binary_sequence.py co-feats "$test_nj" || exit 1
	# combine MFCC & Qdict features
	python self-code/combine_ali.py "$test_nj" "$feats_nj" co-feats || exit 1
	# generate ark, scp files from txt files
	./self-code/txt_ark_scp_transform.sh co-feats "$feats_nj" || exit 1
	# combine all scps to one scp
	python self-code/together.py co-feats "$feats_nj" || exit 1

	echo "MFCC & Qdict features are combined now!"
fi

if [ $stage -le 3 ]; then
	# generate posteriorgrams
	echo ============================================================================
	echo "                      Generate Posteriorgrams                            "
	echo ============================================================================
	feature_transform=exp/dnn4_pretrain-dbn_dnn/final.feature_transform
	nnet=exp/dnn4_pretrain-dbn_dnn/final.nnet
	feats="ark,s,cs:copy-feats scp:co-feats/com_mfcc_test.scp ark:- |"
	nnet-forward --feature-transform=$feature_transform --use-gpu=yes "$nnet" "$feats" ark,t:data/test/test_post-softmax.txt || exit 1

	echo "Posteriorgrams Done! The file is data/test/test_post-softmax.txt"
fi

if [ $stage -le 4 ]; then
	echo ============================================================================
	echo "                      Benchmark APM by decoding                           "
	echo ============================================================================
	[ -d exp/dnn4_pretrain-dbn_dnn/decode_test ] && rm -rf exp/dnn4_pretrain-dbn_dnn/decode_test
	dataset=data/test
	rm -rf $dataset/split*

	# replace raw-feats with combined feats 
	mv $dataset/feats.scp $dataset/raw_feats.scp
	cp co-feats/com_mfcc_test.scp $dataset/feats.scp
	echo "feats replaced! "

	# generate duration file
	wav-to-duration scp:$dataset/wav.scp ark,t:$dataset/dur.ark || exit 1
	echo "duration file complete!"

	python self-code/phn_sequence_text.py $dataset || exit 1
	# generate stm file
	awk -v dur=$dataset/dur.ark \
	'BEGIN{ 
	 while(getline < dur) { durH[$1]=$2; } 
	 print ";; LABEL \"O\" \"Overall\" \"Overall\"";
	 print ";; LABEL \"F\" \"Female\" \"Female speakers\"";
	 print ";; LABEL \"M\" \"Male\" \"Male speakers\""; 
	} 
	{ wav=$1; spk=gensub(/_.*/,"",1,wav); $1=""; ref=$0;
	 gender=(substr(spk,0,1) == "f" ? "F" : "M");
	 printf("%s 1 %s 0.0 %f <O,%s> %s\n", wav, spk, durH[wav], gender, ref);
	}
	' $dataset/phn_text > $dataset/stm || exit 1
	echo "stm file complete!"

	# generate glm file
	echo ';; empty.glm
	[FAKE]     =>  %HESITATION     / [ ] __ [ ] ;; hesitation token
	' > $dataset/glm
	echo "glm file complete!"

	# decoding
	steps/nnet/decode.sh --nj $decode_nj --cmd "$decode_cmd" --acwt 0.2 \
	    exp/mono/graph $dataset exp/dnn4_pretrain-dbn_dnn/decode_test || exit 1

	echo "Decoding complete! The result is in 'exp/dnn4_pretrain-dbn_dnn/decode_test/score*' !"
fi

echo Finish
exit 0