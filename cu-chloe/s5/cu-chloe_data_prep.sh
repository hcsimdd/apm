#!/bin/bash

CU_CHLOE=$1

code_dir=./self-code

[ ! -d data/test ] && mkdir -p data/test
# prepare wave file
python $code_dir/create_wav_scp.py $CU_CHLOE
echo "wave files complete!"

# prepare text file
python $code_dir/create_text.py data/test # only text file is useful
# After this, you should modify some multi-pronunciation words in text file by hand according to the lexicon and context.
echo "text file complete!"

# prepare utt2spk file
python $code_dir/create_utt2spk.py data/test
echo "utt2spk file complete!"

# prepare spk2utt file
utils/utt2spk_to_spk2utt.pl data/test/utt2spk > data/test/spk2utt
echo "spk2utt file complete!"
