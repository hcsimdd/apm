def num2binary(num):
	bin_arr = []
	while(num):
		bin_arr.append(int(num%2))
		num = int(num/2)
	while(len(bin_arr)<6):
		bin_arr.append(0)

	return bin_arr[::-1]

dir = "./co-feats/"
total_copies = 2
for i in range(total_copies):
	rfile = open(dir+"ali_test_phones."+str(i+1)+".txt","r")
	wfile = open(dir+"ali_test_binary."+str(i+1)+".txt","w")
	for line in rfile.readlines():
		text = line.split(';')
		wfile.write(text[0].split(' ')[0]+"  [\n")
		for num_pair in text:
			a = int(num_pair.split(' ')[1])
			b = int(num_pair.split(' ')[2])
			if num_pair == text[-1]:				
				for j in range(b):
					bin_arr = num2binary(a)
					wfile.write("  ")
					for x in bin_arr:
						wfile.write(str(x)+" ")
					if j == b-1:
						wfile.write("]\n")
					else:
						wfile.write("\n")
			else:
				for j in range(b):
					bin_arr = num2binary(a)
					wfile.write("  ")
					for x in bin_arr:
						wfile.write(str(x)+" ")
					wfile.write("\n")
	rfile.close()
	wfile.close()

