#this file copies all the scps into one file
import sys
ldir = sys.argv[1]+"/"

dataset = "test"
com_copies = int(sys.argv[2])
wfile = open(ldir+"com_mfcc_"+dataset+".scp","w")
for i in range(com_copies):
	rfile = open(ldir+"com_mfcc_"+dataset+"."+str(i+1)+".scp","r")
	for line in rfile.readlines():
		wfile.write(line)

	rfile.close()

wfile.close()
