#
Clusters = 90
fdir = "/home/leo/result_kmeans_"+str(Clusters)+"/"
rfile = open(fdir+"phnidprob_in_clusters.txt","r")
wfile = open(fdir+"phones2clusters.txt","w")

phone_list = [[] for i in range(48)]

for line in rfile.readlines():
	cluster, phone = line.split(" ")[1:3]
	phone = phone[:-1]
	phone_list[int(phone)-1].append(cluster)

rfile.close()

rfile = open("data/lang/phones.txt","r")
phone_dict = {}
for line in rfile.readlines():
	value, key = line.split(" ")[:2]
	key = key[:-1]
	phone_dict.update({key:value})

rfile.close()

for i in range(48):
	wfile.write(str(i+1)+" "+phone_dict.get(str(i+1))+": ")
	for cluster in phone_list[i]:
		wfile.write(cluster+" ")
	wfile.write("\n")

wfile.close()
