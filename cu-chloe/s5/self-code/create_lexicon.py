#this file is used to create lexicon file
import os

rfile = open("data/local/corpus.txt","r")
wfile = open("data/local/dict/lexicon.txt","w")
T_dicfile = open("/home/leo/timit/timit/doc/timitdic.txt","r")
C_dicfile = open("CMU_dict","r")
map_61_48_file = open("conf/phones.60-48-39.map","r")
non_phone_file = open("data/local/dict/non_phone.txt","w")

C_dict = {}
T_dict = {}
convert_dict = {}
map_dict = {}

for line in map_61_48_file.readlines():
	if line.find("	")==-1:
		key = line[:-1]
		value = ""
	else:
		key, value = line.split("	")[:-1]
	map_dict.update({key:value})

map_61_48_file.close()

for line in T_dicfile.readlines():
	if line[0]!=";":
		key, value = line.split("  ")
		value = value[1:]
		point = value.find("/")
		value = value[:point]
		T_dict.update({key:value})

T_dicfile.close()

i = 0
for line in C_dicfile.readlines():
	point = line.find(" ")
	key = line[:point]
	value = line[point+1:-1]
	C_dict.update({key:value})
	if i<41:
		convert_dict.update({value:key})
		i = i+1

C_dicfile.close()

wfile.write("sil sil \n")
word1 = []
word2 = []
for i in range(86):
	line = rfile.readline()
	word_list = line.split(" ")[:-1]
	for word in word_list:
		if(word[-1] not in 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'):
			word = word[:-1]
		if word[0] in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
			word = word.lower()
		if T_dict.get(word)==None and word not in word1:
			if C_dict.get(word)!=None:
				word1.append(word)
				wfile.write(word+" ")
				for phone in C_dict.get(word).split(" "):
					phone = convert_dict.get(phone)
					phone = map_dict.get(phone)
					wfile.write(phone+" ")
				wfile.write("\n")
			else:
				if word not in word2:
					word2.append(word)
					non_phone_file.write(word+" \n")

non_phone_file.close()

T_dicfile = open("/home/leo/timit/timit/doc/timitdic.txt","r")

for line in T_dicfile.readlines():
	if line[0]!=";":
		key, value = line.split("  ")
		value = value[1:]
		point = value.find("/")
		value = value[:point]
		wfile.write(key+" ")
		for phone in value.split(" "):
			if phone[-1] in '12345':
				phone = phone[:-1]
			phone = map_dict.get(phone)
			wfile.write(phone+" ")
		wfile.write("\n")


T_dicfile.close()

rfile.close()
wfile.close()

# os.system("sed -i 's/\($\)/\1 /g' data/local/dict/lexicon.txt")
# os.system("sed -i 's/  //g' data/local/dict/lexicon.txt")