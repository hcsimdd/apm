#this file is used to create CHLOE data
. ./path.sh

python ./self-code/create_data.py
#if using filtering, please uncomment the following line
python ./self-code/filtering_data.py 0.1 48.create_posteriorgrams.txt 48
utils/shuffle_list.pl --srand 777 <48.create_posteriorgrams_filtered.txt >48.create_posteriorgrams_filtered_shuffled.txt
python ./self-code/change_format.py 48 48.create_posteriorgrams_filtered_shuffled.txt

# no filtering
# utils/shuffle_list.pl --srand 777 <48.create_posteriorgrams.txt >48.create_posteriorgrams_shuffled.txt
# python ./self-code/change_format.py 48 48.create_posteriorgrams_shuffled.txt