#!/bin/bash

#this file is used to generate posteriorgrams
. ./path.sh

num_state=144
echo "$0 $@"
. utils/parse_options.sh

echo "num_state="$num_state
python ./self-code/gen-posteriorgrams.py $num_state

# utils/shuffle_list.pl --srand 777 <$num_state.test_posteriorgrams.txt >$num_state.test_posteriorgrams_shuffled.txt

python ./self-code/change_format.py $num_state $num_state.test_posteriorgrams.txt
