import sys

def num2binary(num):
	bin_arr = []
	while(num):
		bin_arr.append(int(num%2))
		num = int(num/2)
	while(len(bin_arr)<6):
		bin_arr.append(0)

	return bin_arr[::-1]

fdir = sys.argv[1]+"/"
total_copies = int(sys.argv[2])
splice = 3

for i in range(total_copies):
	rfile = open(fdir+"ali_test_phones."+str(i+1)+".txt","r")
	wfile = open(fdir+"ali_test_binary_tmp."+str(i+1)+".txt","w")
	for line in rfile.readlines():
		text = line.split(';')
		wfile.write(text[0].split(' ')[0]+" ")
		total_pairs = len(text)
		for j in range(total_pairs):
			if j < splice:
				for k in range(splice-j):
					wfile.write(str(1)+" ")
				for k in range(splice+j+1):
					wfile.write(text[k].split(" ")[1]+" ")
			elif j > total_pairs-1-splice:
				for k in range(splice+total_pairs-j):
					wfile.write(text[j-splice+k].split(" ")[1]+" ")
				for k in range(splice+1+j-total_pairs):
					wfile.write(str(1)+" ")
			else:
				for k in range(2*splice+1):
					wfile.write(text[j-splice+k].split(" ")[1]+" ")

			if j == total_pairs-1:
				wfile.write(text[j].split(" ")[2]+" ")
			else:
				wfile.write(text[j].split(" ")[2]+" ; ")

		wfile.write("\n")
	rfile.close()
	wfile.close()

for i in range(total_copies):
	rfile = open(fdir+"ali_test_binary_tmp."+str(i+1)+".txt","r")
	wfile = open(fdir+"ali_test_binary."+str(i+1)+".txt","w")
	for line in rfile.readlines():
		text = line.split(';')
		wfile.write(text[0].split(' ')[0]+"  [\n")
		for num_pair in text:
			a = num_pair.split(' ')[1:8]
			b = num_pair.split(' ')[8]
			bin_arr = []
			for phone in a:
				p_c_list = num2binary(int(phone))
				for p_c in p_c_list:
					bin_arr.append(p_c)
			if num_pair[-1] == "\n":
				for j in range(int(b)):
					wfile.write("  ")
					for x in bin_arr:
						wfile.write(str(x)+" ")
					if j == int(b)-1:
						wfile.write("]\n")
					else:
						wfile.write("\n")
			else:
				for j in range(int(b)):
					wfile.write("  ")
					for x in bin_arr:
						wfile.write(str(x)+" ")
					wfile.write("\n")
	rfile.close()
	wfile.close()





