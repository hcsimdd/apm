#this file is used to create wav.scp from CU-CHLOE corpus
import os
import sys

datadir = sys.argv[1]+"/Recordings/"
wfile = open("data/test/wav.scp","w")
for cate in os.listdir(datadir):
	for people in os.listdir(datadir+cate+"/"):
		for wavfile in os.listdir(datadir+cate+"/"+people+"/"):
			if(wavfile[-1]=="v"):
				wfile.write(people+"_"+wavfile[:-4]+" "+datadir+cate+"/"+people+"/"+wavfile+" "+"\n")

wfile.close()

