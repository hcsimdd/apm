#
fdir = "co-feats/"
data_copies = 2

wfile = open(fdir+"frameid2ali-phone.txt","w")

for i in range(data_copies):
	rfile = open(fdir+"ali_test_phones."+str(i+1)+".txt","r")
	for line in rfile.readlines():
		txt = line.split(";")
		sentenceid = txt[0].split(" ")[0]
		frame = 0
		for num_pair in txt:
			phone = num_pair.split(" ")[1]
			num = int(num_pair.split(" ")[2])
			for j in range(num):
				wfile.write(sentenceid+"_"+str(frame)+" "+phone+" \n")
				frame = frame+1

	rfile.close()
wfile.close()