#this file is used to have a deep look into the posteriorgrams

#Part One
yuzhi = 0.1
amount = 2

rfile = open("test_posteriorgrams_shuffled.txt","r")
wfile = open("diffphone_multi_statelike_"+str(yuzhi)+"_"+str(amount)+".txt","w")
sum = 0
for line in rfile.readlines():
	txt = line.split(' ')
	j = 0
	i = 0
	count = 0
	for num in txt[:-1]:
		if(count==0):
			if(float(num)>yuzhi):
				j = j+1
				if(i%3==0):
					count = 2
				if(i%3==1):
					count = 1
				if(i%3==2):
					count = 0
		else:
			count = count-1
		i = i+1

	if(j>=amount):
		wfile.write(line)
		sum = sum+1
print(sum)

rfile.close()
wfile.close()

#Part Two
rfile = open("diffphone_multi_statelike_"+str(yuzhi)+"_"+str(amount)+".txt","r")
wfile = open("phones_similar.txt","w")
for line in rfile.readlines():
	txt = line.split(' ')
	i = 0
	for num in txt[:-1]:
		if(float(num)>yuzhi):
			wfile.write(str(int(i/3)+1)+":"+num+" ")
		i = i+1
	wfile.write("\n")

rfile.close()
wfile.close()
