#
rfile = open("co-feats/frameid2num.txt","r")
frame_dict = {}
for line in rfile.readlines():
	key, value = line.split(" ")[:2]
	frame_dict.update({key:value})
rfile.close()

rfile = open("co-feats/frameid2ali-phone.txt","r")
wfile = open("co-feats/kmeans-initial.txt","w")

phone_list = []

prephone = "1"
count = 0
for line in rfile.readlines():
	sentenceid, phone = line.split(" ")[:2]
	if phone not in phone_list:
		if phone == prephone:
			count = count+1
			if count == 5:
				phone_list.append(phone)
				wfile.write(frame_dict.get(sentenceid)+" "+sentenceid+" "+phone+" \n")
				count = 0
		else:
			count = 0
	prephone = phone

rfile.close()
wfile.close()