#this file is used to filter raw data.
import sys

threshold = float(sys.argv[1])
rfilename = sys.argv[2]
num_state = int(sys.argv[3])
wfilename = rfilename[:-4]+"_filtered.txt"

print("filtering!\n")
print("threshold = "+str(threshold)+"\n")
print("rfilename = "+str(rfilename)+"\n")
print("wfilename = "+str(wfilename)+"\n")
print("num_state = "+str(num_state)+"\n")

rfile = open(rfilename,"r")
wfile = open(wfilename,"w")

for line in rfile.readlines():
	txt = line.split(' ')[:-1]
	total = 0
	for num in txt:
		if(float(num)>threshold):
			total = total+float(num)
	for i in range(num_state):
		if(float(txt[i])<=threshold):
			txt[i] = str(0)
		else:
			txt[i] = str(float(txt[i])/total)
		wfile.write(txt[i]+" ")
	wfile.write("\n")

rfile.close()
wfile.close()