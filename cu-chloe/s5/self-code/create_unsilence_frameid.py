
fdir = "co-feats/"
data_copies = 2

wfile = open(fdir+"unsilence-frameid.txt","w")

for i in range(data_copies):
	rfile = open(fdir+"ali_test_phones."+str(i+1)+".txt","r")
	for line in rfile.readlines():
		txt = line.split(";")
		sentenceid = txt[0].split(" ")[0]
		num_frame = 0
		for num_pair in txt:
			a = int(num_pair.split(" ")[1])
			b = int(num_pair.split(" ")[2])
			if a == 1:
				num_frame = num_frame+b
			else:
				for j in range(b):
					wfile.write(sentenceid+"_"+str(num_frame)+" \n")
					num_frame = num_frame+1

	rfile.close()

wfile.close()
