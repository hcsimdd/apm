#!/bin/bash

text=$1

sed -i 's/\t\t/ /g' $text
sed -i 's/\t/ /g' $text
sed -i 's/\($\)/\1 /g' $text
sed -i 's/  / /g' $text