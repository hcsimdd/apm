#this file is used to change format.
import sys

num_state = int(sys.argv[1])
rfilename = sys.argv[2]
wfilename = rfilename[:-3]+"csv"
rfile = open(rfilename,"r")
wfile = open(wfilename,"w")

print("changing format!\n")
print("num_state: "+str(num_state)+"\n")
print("rfilename: "+str(rfilename)+"\n")
print("wfilename: "+str(wfilename)+"\n")


# for i in range(num_state-1):
# 	wfile.write("state"+str(i)+",")

# wfile.write("state"+str(num_state-1)+"\n")

for line in rfile.readlines():
	txt = line.split(' ')
	for num in txt[:-2]:
		wfile.write(str(num)+",")
	wfile.write(str(txt[-2])+"\n")

rfile.close()
wfile.close()