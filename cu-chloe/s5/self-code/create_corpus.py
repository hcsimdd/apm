#this file is used to create corpus.txt 
import os

datadir = "/home/leo/CU-CHLOE/Recordings/Cantonese/MC002/"
TIMITdir = "/home/leo/timit/timit/doc/"
wfile = open("data/local/corpus.txt","w")
for wavfile in os.listdir(datadir):
	if(wavfile[-1]=="t" and wavfile[0]!="s"):
		rfile = open(datadir+wavfile,"r")
		corpus = rfile.readline()
		if(corpus[-1]=="\n"):
			wfile.write(corpus)
		else:
			wfile.write(corpus+"\n")

		rfile.close()

rfile = open(TIMITdir+"prompts.txt","r")

for line in rfile.readlines():
	if(line[0]!=";"):
		txt = line.split("(")[0]
		wfile.write(txt+"\n")

rfile.close()
wfile.close()

os.system("./text_format.sh data/local/corpus.txt")

