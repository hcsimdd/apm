#this file is used to combine alis
import sys

dataset = "test"
binary_copies = int(sys.argv[1])
mfcc_copies = int(sys.argv[2])
fdir = sys.argv[3]

wfile = open(fdir+"/ali_"+dataset+"_binary.txt","w")
for i in range(binary_copies):
	rfile = open(fdir+"/ali_"+dataset+"_binary."+str(i+1)+".txt","r")
	for line in rfile.readlines():
		wfile.write(line)

	rfile.close()

wfile.close()

rfile1 = open(fdir+"/ali_"+dataset+"_binary.txt","r")
lines1 = rfile1.readlines()
j = 0
for i in range(mfcc_copies):
	rfile = open(fdir+"/raw_mfcc_"+dataset+"."+str(i+1)+".txt","r")
	wfile = open(fdir+"/com_mfcc_"+dataset+"."+str(i+1)+".txt","w")
	for line in rfile.readlines():
		if(line[0]!=' '):
			wfile.write(line)
		elif(line[-2]==']'):
			wfile.write(line[:-2]+lines1[j][2:])
		else:
			wfile.write(line[:-1]+lines1[j][2:])

		j = j+1

	rfile.close()
	wfile.close()

rfile1.close()
