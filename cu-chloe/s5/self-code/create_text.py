#this file is used to create text file 
import os
import sys
setdir = sys.argv[1]+"/"
rfile = open(setdir+"wav.scp","r")
wfile = open(setdir+"text1","w")

for line in rfile.readlines():
	txt = line.split(" ")
	textfile = open(txt[1][:-4]+".txt","r")
	text = textfile.readline()
	if(text[-1]=="\n"):
		wfile.write(txt[0]+" "+text)
	else:
		wfile.write(txt[0]+" "+text+"\n")
	textfile.close()

rfile.close()
wfile.close()

rfile = open(setdir+"text1","r")
wfile = open(setdir+"text2","w")

for line in rfile.readlines():
	line = line.replace("\t"," ")
	line = line.replace("\r"," ")
	line = line.replace("  "," ")
	if line[-2] != " ":
		line = line[:-1]+" \n"
	wfile.write(line)

rfile.close()
wfile.close()

rfile = open(setdir+"text2","r")
wfile = open(setdir+"text3","w")

word_list = ["etc.","Mr.","Mrs."]

for line in rfile.readlines():
	txt = line.split(" ")[:-1]
	wfile.write(txt[0]+" ")
	for word in txt[1:]:
		if word[0] == '"':
			word = word[1:]
		while word[-1] in ',?!;:"':
			word = word[:-1]
		if word[-1] == '.':
			if word not in word_list:
				word = word[:-1]
		word = word.lower()
		wfile.write(word+" ")
	wfile.write("\n")

rfile.close()
wfile.close()

rfile = open(setdir+"text3","r")
wfile = open(setdir+"text","w")

for line in rfile.readlines():
	txt = line.split(" ")[:-1]
	filename = txt[0]
	wfile.write(filename+" ")
	if filename[6] in 'cm':
		for word in txt[1:]:
			wfile.write("sil "+word+" ")
		wfile.write("sil \n")
	else:
		wfile.write("sil ")
		for word in txt[1:]:
			wfile.write(word+" ")
		wfile.write("sil \n")

rfile.close()
wfile.close()

os.system("rm "+setdir+"text1")
os.system("rm "+setdir+"text2")
os.system("rm "+setdir+"text3")
# human modify some multi-pronunciation words
