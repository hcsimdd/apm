#! /bin/bash

. ./path.sh

#txt2ark&scp
dir=$1
dataset=test
com_copies=$2
for((i=1;i<$com_copies+1;i++));do
    copy-feats ark,t:$dir/com_mfcc_$dataset.$i.txt ark,scp:$dir/com_mfcc_$dataset.$i.ark,$dir/com_mfcc_$dataset.$i.scp
done
